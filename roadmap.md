# Roadmap 
## Versions 
0.1 - working base functionality
0.1.1 - returning results instead of unwrapping in functions
0.2 - dynamic settings using command-line arguments, first public/open source release
## Planned features
- #1 configuration file support
- #2 multi monitor support
- #3 pairing devices
- #4 security (dep. on #3)
- #5 setup wizard (dep. on #1) 
    - PCRE regex for matching active outputs `(?!Screen)(?<out>^\S+)\s.+?(?<res>\d+x\d+)(?<pos>\+\d+\+\d+).*`
    - PCRE regex for matching inactive outputs `(?!Screen)(?<out>^\S+)(?!.*?\d)`
- #6 optional disabling of client ready trigger or using stdout of a command ran by bash as a trigger, changing thinkpad dock trigger to an optional feature, using a trait for the trigger for easier extending
- #7 add automatic resolution negotiation

# New one
## Purpose
ReScreen is a tool allowing you to use a laptop as an additional screen for your PC without any special hardware requirements. 

## Connecting
- in pairing mode clients announce themselves every 5 seconds using broadcast packets to the LAN (can be disabled by a flag), the announcement contains a randomly generated three-word identifier (color adjective noun) and possibly resolution
- server has a configuration interface ([using this](https://github.com/fdehau/tui-rs)) which shows a list of detected clients and has an option for manual connections
- each device has a permanent UUID
- pairing devices and connecting are separate steps
- server has a `rescreen setup` subcommand for the setup interface
- each server <-> client pair has a shared UUID, if one no longer recognizes the UUID, the pair becomes invalid
- abstract the network backend away so it can be replaced easily in future

## General
- setting profiles support
- abstracted away VNC client/server interface allowing for a different backend (traits)
- settings system that can be used by any backend, probably no support for CLI arguments except specifying config file to use

## Modularization
- single crate or client/common/server

## External application handling
- 
