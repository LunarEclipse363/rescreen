# ReScreen
Adding a virtual screen to a device running Linux/X11 which is then livestreamed to another computer via VNC.
The use case is, for example, an old laptop which could function as a free extra screen.

## Status
The current status is a proof-of-concept, which may or may not work depending on the hardware/drivers the server is ran on.
Unfortunately, with no proper API for adding virutal screens, and the functionality being okayish at best, and completely broken at worst (like on my current computer), I decided there is no point in maintaining this project.

## Structure
The program consists of a server (the computer a virtual screen of which is being shared) and a client (the computer which displays the virtual screen).

It is supposed to be used by first running the client, which will wait for a UDP broadcast packet containing a special string.
Then, when the server is ran, it initializes the virtual screen, launches a VNC server, and sends out the UDP broadcast packet.
After receiving the UDP broadcast packet, the client launches a VNC client, pointing it to the source IP of the broadcast.
This functions as a rudimentary autodiscovery protocol (which should ideally be replaced with one that provides better authentication and reliability).
