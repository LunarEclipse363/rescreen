# Server
## Packages
- `xrandr x11vnc`
## Commands
### Init
1. `xrandr --addmode <output> <mode>`
2. `xrandr --output <output> --mode <mode> [position]`
2. `sudo x11vnc -display <display> -auth <auth> -clip <mode>+<xoffset>+<yoffset> -forever -cursor none -noxfixes -noxdamage
### Deinit
`xrandr --off <output>`
`xrandr --delmode <output> <mode>`

# Client
## Packages
- `tightvnc`
## Commands
vncviewer -viewonly -fullscreen -x11cursor <address>
