use clap::{App, Arg};
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr, UdpSocket};
use std::process::Command;

fn main() {
    let config = get_config();
    println!("Waiting for start signal...");
    loop {
        let socket = UdpSocket::bind((Ipv4Addr::UNSPECIFIED, config.port)).unwrap();
        let mut buf = [0u8; 21];
        let (_, src) = socket.recv_from(&mut buf).unwrap();
        if buf == "RESCREEN_SERVER_READY".as_bytes() {
            println!("Received ready message from server, launching vnc client...");
            let vnc_client_output = Command::new("vncviewer")
                .args(&[
                    "-viewonly",
                    "-fullscreen",
                    "-x11cursor",
                    format!("{}:{}", src.ip(), config.vnc_port).as_ref(),
                ])
                .output()
                .unwrap();
            break;
        }
    }
}

#[derive(Debug)]
struct Config {
    port: u16,
    vnc_addr: Option<String>,
    vnc_port: u16,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            port: 1337u16,
            vnc_addr: None,
            vnc_port: 5900u16,
        }
    }
}

fn get_config() -> Config {
    let mut config = Config::default();
    let args = App::new("ReScreen Client")
        .version("0.1a")
        .author(clap::crate_authors!())
        .about("") // TODO
        .arg(
            Arg::with_name("port")
                .short("s")
                .long("port")
                .value_name("PORT")
                .help("Sets a custom listening port for the UDP socket")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("vnc_addr")
                .short("a")
                .long("vnc-address")
                .value_name("ADDR")
                .help("Sets the address for the vnc client")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("vnc_port")
                .short("p")
                .long("vnc-port")
                .value_name("PORT")
                .help("Sets the port for the vnc client")
                .takes_value(true),
        )
        .get_matches();
    if let Some(x) = args.value_of("port") {
        config.port = x.parse().unwrap()
    }
    if let Some(x) = args.value_of("vnc_addr") {
        if let Err(e) = x.parse::<IpAddr>() {
            panic!("Error parsing IP address: {}", e)
        }
        config.vnc_addr = Some(x.parse().unwrap())
    }
    if let Some(x) = args.value_of("vnc_port") {
        config.vnc_port = x.parse().unwrap()
    }
    config
}
