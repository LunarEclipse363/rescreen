use clap::{App, Arg};
use std::error::Error as StdError;
use std::fmt;
use std::net::{Ipv4Addr, UdpSocket};
use std::process::Command;

const OUTPUT: &'static str = "HDMI-A-0";

fn main() {
    println!("ReScreen Server v0.1a starting...");
    let config = get_config();
    if !config.deinit_only {
        println!("Initializing screen and launching the vnc server...");
        let mut handle = init().unwrap();
        println!("Sending start signal to client(s)");
        send_start_signal().unwrap();
        handle.wait().unwrap();
    }
    println!("Deinitializing screen...");
    deinit().unwrap();
}

struct Config {
    deinit_only: bool
}

impl std::default::Default for Config {
    fn default() -> Self {
        Config {
            deinit_only: false
        }
    }
}

fn get_config() -> Config {
    let mut config = Config::default();
    let args = App::new("ReScreen Server")
        .version("0.1a")
        .author(clap::crate_authors!())
        .about("Program allowing you to create virtual displays and stream their content to another device running the client")
        .arg(Arg::with_name("config")
             .short("c")
             .long("config")
             .takes_value(true)
             .value_name("FILE")
             .help("Sets a config file")
             //.required_unless_all(&[""]) // TODO: add required
             //.required_unless("wizard")) // TODO: add a setup wizard
             )
         //.arg(Arg::with_name("device")) 
        .arg(Arg::with_name("deinit")
            .short("d")
            .long("deinit")
            .takes_value(false)
            .help("Deinitialize and exit (necessary after a failure)")
            )
        .get_matches();

    if args.is_present("deinit") {
        config.deinit_only = true;
    }

    config
}

fn init() -> Result<std::process::Child, Box<dyn StdError>> {
    // "1360x768_60.00"   84.75  1360 1432 1568 1776  768 771 781 798 -hsync +vsync
    let xrandr_new_mode_output = Command::new("xrandr")
        .args(&[
            "--newmode",
            "1360x768_60.00",
            "84.75",
            "1360",
            "1432",
            "1568",
            "1776",
            "768",
            "771",
            "781",
            "798",
            "-hsync",
            "+vsync",
        ])
        .output()?;
    handle_exit_codes(&xrandr_new_mode_output)?;
    let xrandr_add_mode_output = Command::new("xrandr")
        .args(&["--addmode", OUTPUT, "1360x768_60.00"])
        .output()?;
    handle_exit_codes(&xrandr_add_mode_output)?;
    let xrandr_set_mode_output = Command::new("xrandr")
        .args(&[
            "--output",
            OUTPUT,
            "--mode",
            "1360x768_60.00",
            "--left-of",
            "eDP",
        ])
        .output()?;
    handle_exit_codes(&xrandr_set_mode_output)?;
    let vnc_server_handle = Command::new("sudo")
        .args(&["bash", "-c"])
        .arg("x11vnc -display :0 -auth guess -cursor none -clip 1360x768+0+0")
        .spawn()?;
    std::thread::sleep(std::time::Duration::from_millis(10000)); // TODO: Implement better way of waiting for sudo, possibly use something entirely different
    Ok(vnc_server_handle)
}

fn deinit() -> Result<(), Box<dyn StdError>> {
    let xrandr_off_output = Command::new("xrandr")
        .args(&["--output", OUTPUT, "--off"])
        .output()?;
    handle_exit_codes(&xrandr_off_output)?;
    let xrandr_del_mode_output = Command::new("xrandr")
        .args(&["--delmode", OUTPUT, "1360x768_60.00"])
        .output()?;
    handle_exit_codes(&xrandr_del_mode_output)?;
    let xrandr_rmmode_output = Command::new("xrandr")
        .args(&["--rmmode", "1360x768_60.00"])
        .output()?;
    handle_exit_codes(&xrandr_rmmode_output)?;
    Ok(())
}

fn handle_exit_codes(out: &std::process::Output) -> Result<(), Error> {
    if let Some(code) = out.status.code() {
        if code != 0 {
            eprintln!("Command error: {:#?}", out);
            return Err(Error::NonZeroExit);
        }
    }
    Ok(())
}

fn send_start_signal() -> Result<(), std::io::Error> {
    let socket = UdpSocket::bind((Ipv4Addr::UNSPECIFIED, 0u16))?;
    socket.set_broadcast(true)?;
    socket.send_to(
        "RESCREEN_SERVER_READY".as_bytes(),
        (Ipv4Addr::BROADCAST, 1337u16),
    )?;
    Ok(())
}

#[derive(Debug)]
enum Error {
    NonZeroExit,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::NonZeroExit => f.write_str("NonZeroExit")?,
        }
        Ok(())
    }
}

impl StdError for Error {}
